<?php

namespace Drupal\media_entity_imagemagick\Plugin\media\Source;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\file\FileInterface;
use Drupal\imagemagick\ImagemagickFormatMapperInterface;
use Drupal\media\MediaInterface;
use Drupal\media\Plugin\media\Source\Image;
use Symfony\Component\DependencyInjection\ContainerInterface;
use function explode;
use function is_file;
use function strtolower;

/**
 * Provides media type plugin for Imagemagick.
 *
 * @MediaSource(
 *   id = "imagemagick",
 *   label = @Translation("Imagemagick"),
 *   description = @Translation("Provides business logic and metadata for local image manipulation using the imagemagick module."),
 *   allowed_field_types = {"file", "image"},
 *   default_thumbnail_filename = "generic.png"
 * )
 */
class Imagemagick extends Image {

  const DEFAULT_OUTPUT_EXTENSION = 'jpg';
  const DEFAULT_OPERATION = 'convert';
  const DEFAULT_STATE = FALSE;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * File storage.
   *
   * @var \Drupal\file\FileStorageInterface
   */
  protected $fileStorage;

  /**
   * Format mapper.
   *
   * @var \Drupal\imagemagick\ImagemagickFormatMapperInterface
   */
  protected $formatMapper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('config.factory'),
      $container->get('image.factory'),
      $container->get('file_system'),
      $container->get('logger.factory'),
      $container->get('imagemagick.format_mapper')
    );
  }

  /**
   * Constructs a new class instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Image\ImageFactory $image_factory
   *   The image factory.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\imagemagick\ImagemagickFormatMapperInterface $formatMapper
   *   Format mapper.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $entity_field_manager,
    FieldTypePluginManagerInterface $field_type_manager,
    ConfigFactoryInterface $config_factory,
    ImageFactory $image_factory,
    FileSystemInterface $file_system,
    LoggerChannelFactoryInterface $logger_factory,
    ImagemagickFormatMapperInterface $formatMapper
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $entity_field_manager,
      $field_type_manager,
      $config_factory,
      $image_factory,
      $file_system
    );

    $this->fileStorage = $entity_type_manager->getStorage('file');
    $this->logger = $logger_factory->get('media_entity_imagemagick');
    $this->formatMapper = $formatMapper;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'source_field' => '',
      'custom_thumbnail_field' => '',
      'imagemagick_operation' => static::DEFAULT_OPERATION,
      'apply_operation' => static::DEFAULT_STATE,
      'conversion_output_type' => static::DEFAULT_OUTPUT_EXTENSION,
    ];
  }

  /**
   * Extract a file from the media according to the given config.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media.
   * @param string $configName
   *   The name of the config, e.g source_field, custom_thumbnail_field.
   *
   * @return \Drupal\file\FileInterface|null
   *   The file or NULL.
   */
  private function extractFileViaConfig(MediaInterface $media, $configName) {
    if (
      ($field = $this->extractFieldViaConfig($media, $configName))
      && !$field->isEmpty()
    ) {
      return $field->entity;
    }

    return NULL;
  }

  /**
   * Extract a field from the media according to the given config.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media.
   * @param string $configName
   *   The name of the config, e.g source_field, custom_thumbnail_field.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|null
   *   The field or NULL.
   */
  private function extractFieldViaConfig(MediaInterface $media, $configName) {
    if (
      !empty($this->configuration[$configName])
      && $media->hasField($this->configuration[$configName])
      && ($field = $media->get($this->configuration[$configName]))
    ) {
      return $field;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $name) {
    // Get the file and image data.
    /** @var \Drupal\file\FileInterface|null $file */
    $file = $this->extractFileViaConfig($media, 'custom_thumbnail_field');

    if ($file === NULL) {
      $file = $this->extractFileViaConfig($media, 'source_field');
    }

    // If the source field is not required, it may be empty.
    if ($file === NULL) {
      return parent::getMetadata($media, $name);
    }

    $image = $this->imageFactory->get($file->getFileUri());

    switch ($name) {
      case static::METADATA_ATTRIBUTE_WIDTH:
        return $image->getWidth() ?: NULL;

      case static::METADATA_ATTRIBUTE_HEIGHT:
        return $image->getHeight() ?: NULL;

      case 'thumbnail_uri':
        return $this->getThumbnail($file);

      case 'thumbnail_alt_value':
        return $this->getThumbnailAltValue($media) ?: parent::getMetadata($media, $name);
    }

    return parent::getMetadata($media, $name);
  }

  /**
   * Returns the thumbnail alt value.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity.
   *
   * @return string|null
   *   The alt value or NULL.
   */
  public function getThumbnailAltValue(MediaInterface $media) {
    $field = $this->extractFieldViaConfig($media, 'custom_thumbnail_field');

    if ($field === NULL) {
      $field = $this->extractFieldViaConfig($media, 'source_field');
    }

    return $field->alt ?: NULL;
  }

  /**
   * Add custom thumbnail source field to the form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The state.
   */
  protected function addCustomThumbnailSource(array &$form, FormStateInterface $form_state) {
    $options = $this->getSourceFieldOptions();
    $form['custom_thumbnail_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Custom thumbnail field'),
      '#default_value' => $this->configuration['custom_thumbnail_field'],
      '#empty_option' => $this->t('- Select -'),
      '#options' => $options,
      '#description' => $this->t('Field on media entity that stores a custom thumbnail. If the selected field has an uploaded image then the no imagemagick operation will be executed. You can create a bundle without selecting a value for this dropdown initially. This dropdown can be populated after adding fields to the bundle.'),
    ];

    if (!empty($this->configuration['custom_thumbnail_field']) && $form_state->get('operation') === 'edit') {
      $form['custom_thumbnail_field']['#access'] = FALSE;
      $fields = $this->entityFieldManager->getFieldDefinitions('media', $form_state->get('type')->id());
      $form['custom_thumbnail_field_message'] = [
        '#markup' => $this->t('%field_name field is used to store the custom thumbnail information.', [
          '%field_name' => $fields[$this->configuration['custom_thumbnail_field']]->getLabel(),
        ]),
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $conversionOptions = [];

    foreach ($this->formatMapper->getEnabledFormats() as $format) {
      $conversionOptions[strtolower($format)] = $this->t('@name', [
        '@name' => $this->formatMapper->getMimeTypeFromFormat($format),
      ]);
    }

    // If there are no applicable formats, set 'Apply' to false and disable it.
    // Also inform the user about this.
    if (empty($conversionOptions)) {
      $this->messenger()->addError($this->t('No available output types found.'));
      $operationValue = $this::DEFAULT_STATE;
    }
    else {
      $operationValue = empty($this->configuration['apply_operation']) ? $this::DEFAULT_STATE : $this->configuration['apply_operation'];
    }

    $form = parent::buildConfigurationForm($form, $form_state);
    $this->addCustomThumbnailSource($form, $form_state);

    // @todo: maybe add target field.
    $form['imagemagick_operation'] = [
      '#type' => 'select',
      '#title' => $this->t('Operation'),
      '#description' => $this->t('The operation to be applied to the image.'),
      '#default_value' => empty($this->configuration['imagemagick_operation']) ? $this::DEFAULT_OPERATION : $this->configuration['imagemagick_operation'],
      '#options' => [
        'convert' => $this->t('Convert'),
      ],
      // @todo: Get operations from imagemagick dynamically.
      '#disabled' => TRUE,
    ];

    $form['apply_operation'] = [
      '#type' => 'select',
      '#title' => $this->t('Modify using imagemagick'),
      '#description' => $this->t('Whether to apply the operation to the image.'),
      '#default_value' => $operationValue,
      '#options' => [
        FALSE => $this->t('No'),
        TRUE => $this->t('Yes'),
      ],
      '#disabled' => empty($conversionOptions),
    ];

    $form['conversion_output_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type of output'),
      '#description' => $this->t('The type of the result file.'),
      '#default_value' => empty($this->configuration['conversion_output_type']) ? $this::DEFAULT_OUTPUT_EXTENSION : $this->configuration['conversion_output_type'],
      '#options' => $conversionOptions,
      '#disabled' => empty($conversionOptions) || !(bool) $this->configuration['apply_operation'],
    ];

    return $form;
  }

  /**
   * Gets the thumbnail image URI based on a file entity.
   *
   * @param \Drupal\file\FileInterface $file
   *   A file entity.
   *
   * @return string
   *   File URI of the thumbnail image or NULL if there is no specific icon.
   */
  public function getThumbnail(FileInterface $file) {
    $updatedFile = $this->applyOperation($file);

    /** @var \Drupal\file\FileInterface $file */
    if ($updatedFile) {
      return $updatedFile->getFileUri();
    }

    $iconBase = $this->configFactory->get('media.settings')->get('icon_base_uri');

    // We try to automatically use the most specific icon present in the
    // $iconBase directory, based on the MIME type. For instance, if an
    // icon file named "pdf.png" is present, it will be used if the file
    // matches this MIME type.
    $mimeType = $file->getMimeType();
    $mimeType = explode('/', $mimeType);

    $iconNames = [
      $mimeType[0] . '--' . $mimeType[1],
      $mimeType[1],
      $mimeType[0],
    ];
    foreach ($iconNames as $iconName) {
      $thumbnail = $iconBase . '/' . $iconName . '.png';
      if (is_file($thumbnail)) {
        return $thumbnail;
      }
    }

    return $file->getFileUri();
  }

  /**
   * Try to apply the set operation to the supplied image.
   *
   * The operation (and additional parameters) come from config.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file.
   *
   * @return \Drupal\file\FileInterface|bool
   *   The result file, or FALSE on failure.
   */
  protected function applyOperation(FileInterface $file) {
    $params = [
      'extension' => $this->configuration['conversion_output_type'],
    ];

    // Load the source as an Image instance.
    /** @var \Drupal\Core\Image\ImageInterface $image */
    $image = $this->imageFactory->get($file->getFileUri());

    // Note: We can add custom arguments to the toolkit:
    // $image->getToolkit()->addArgument().
    // E.g -flatten and stuff like that.
    // Note, these are only image options, not settings for convert
    // meaning that "convert inputimage options outputimage" structure
    // will be used by Imagemagick, so stugg like -density won't really work.
    // At least for ImageMagick 6.7.7-10 as I use that currently.
    //
    // Try to apply the operation.
    if (!$image->apply($this->configuration['imagemagick_operation'], $params)) {
      $this->logger->error(
        'The @op operation could not be applied to the @fname file with ID @fid.', [
          '@op' => $this->configuration['imagemagick_operation'],
          '@fname' => $file->getFilename(),
          '@fid' => $file->id(),
        ]
      );

      return FALSE;
    }

    // @todo: It would likely be better to remove the original extension.
    // Save the image to the same place as the original, but suffix it.
    $newLocation = $image->getSource() . '-altered.' . $this->configuration['conversion_output_type'];

    if (!$image->getToolkit()->save($newLocation)) {
      $this->logger->error('Saving the @fname file to the new location ( @loc ) has failed.', [
        '@fname' => $file->getFilename(),
        '@loc' => $newLocation,
      ]);

      return FALSE;
    }

    /** @var \Drupal\file\FileInterface $newFile */
    $newFile = $this->fileStorage->create([
      'uri' => $newLocation,
    ]);

    try {
      $newFile->save();
    }
    catch (EntityStorageException $e) {
      // @todo: Maybe remove the created file.
      $this->logger->error('Could not store the file.');
      return FALSE;
    }

    return $newFile;
  }

}
